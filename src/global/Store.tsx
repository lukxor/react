import React from "react";
import { IAction } from "config/interfaces";

const initialState = {};

export const Store = React.createContext(initialState);

export function reducer(state: any, action: IAction): any {
  switch (action.type) {
    default:
      return state;
  }
}

/* istanbul ignore next */
export function StoreProvider(props: any): JSX.Element {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <Store.Provider value={{ state, dispatch }}>
      {props.children}
    </Store.Provider>
  );
}
